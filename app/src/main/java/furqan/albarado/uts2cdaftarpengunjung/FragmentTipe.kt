package furqan.albarado.uts2cdaftarpengunjung

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_tipe.view.*

class FragmentTipe : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnDelete ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnUpdate ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }

    }

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v :View
    lateinit var builder : AlertDialog.Builder
    var idTipe : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()

        v = inflater.inflate(R.layout.frag_data_tipe,container,false)
        v.btnUpdate.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnDelete.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsTipe.setOnItemClickListener(itemClick)

        return v
    }

    fun showDataTipe(){
        val cursor : Cursor = db.query("tipe", arrayOf("tipe_pengunjung", "id_tipe as _id"),
            null, null, null,null,"tipe_pengunjung asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_tipe,cursor,
            arrayOf("_id","tipe_pengunjung"), intArrayOf(R.id.txldTipe, R.id.txTipe),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsTipe.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataTipe()
    }

    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idTipe = c.getString(c.getColumnIndex("_id"))
        v.edTipe.setText(c.getString(c.getColumnIndex("tipe_pengunjung")))
    }

    fun insertDataTipe(tipePengunjung : String){
        var cv : ContentValues = ContentValues()
        cv.put("tipe_pengunjung",tipePengunjung)
        db.insert("tipe", null,cv)
        showDataTipe()
    }

    fun updateDataTipe(tipePengunjung: String, idTipe: String){
        var cv : ContentValues = ContentValues()
        cv.put("tipe_pengunjung", tipePengunjung)
        db.update("tipe",cv,"id_tipe = $idTipe",null)
        showDataTipe()
    }

    fun deleteDataTipe(idTipe : String){
        db.delete("tipe","id_tipe = $idTipe", null)
        showDataTipe()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataTipe(v.edTipe.text.toString())
        v.edTipe.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataTipe(v.edTipe.text.toString(), idTipe)
        v.edTipe.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataTipe(idTipe)
        v.edTipe.setText("")
    }
}