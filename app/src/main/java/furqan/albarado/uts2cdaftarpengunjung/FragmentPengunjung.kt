package furqan.albarado.uts2cdaftarpengunjung

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kunjungan.*
import kotlinx.android.synthetic.main.frag_data_kunjungan.view.*
import kotlinx.android.synthetic.main.frag_data_kunjungan.view.spinner2
import kotlinx.android.synthetic.main.frag_data_kunjungan.view.spinner3
import kotlinx.android.synthetic.main.frag_data_tipe.view.*
import kotlinx.android.synthetic.main.frag_data_kegiatan.view.*

class FragmentPengunjung : Fragment(), View.OnClickListener {

    val TipeSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner2.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = adapterTipe.getItem(position) as Cursor
            namaTipe = c.getString(c.getColumnIndex("_id"))
        }
    }

    val KegSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner3.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = adapterKeg.getItem(position) as Cursor
            namaKeg = c.getString(c.getColumnIndex("_id"))
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnDeleteK -> {
                dialog.setTitle("Konfirmasi").setMessage("Yakin menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnUpdateK ->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btnInsertK ->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()

            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var adapterTipe: SimpleCursorAdapter
    lateinit var adapterKeg: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaKun : String=""
    var namaTipe : String=""
    var namaKeg : String=""
    var arrTipe = ArrayList<String>()
    var arrKeg = ArrayList<String>()
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.frag_data_kunjungan,container,false)
        db = thisParent.getDBObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btnDeleteK.setOnClickListener(this)
        v.btnInsertK.setOnClickListener(this)
        v.btnUpdateK.setOnClickListener(this)
        v.spinner2.onItemSelectedListener = TipeSelect
        v.spinner3.onItemSelectedListener = KegSelect
        v.lsKunjungan.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataKun("")
        showDataTipe()
        showDataKeg()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        v.edKodeKunjungan.setText(c.getString(c.getColumnIndex("_id")))
        v.edNamaKunjungan.setText(c.getString(c.getColumnIndex("nama")))
        namaTipe = c.getString(c.getColumnIndex("tipe_pengunjung"))
        namaKeg = c.getString(c.getColumnIndex("nmkegiatan"))
        v.spinner2.setSelection(getIndex(v.spinner2,namaTipe,arrTipe))
        v.spinner3.setSelection(getIndex(v.spinner3,namaKeg,arrKeg))
    }

    fun getIndex(spinner: Spinner, myString: String, arrayL: ArrayList<String>): Int {
        var a = spinner.count
        Log.d("Output","Jumlah item dalam array = $a")
        var b : String = ""
        for (i in 0 until a) {
            b = arrayL.get(i)
            Log.d("Output","Index ke $i = $b")
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }


    fun showDataKun(kdKun : String) {
        var sql = ""
        if (!kdKun.trim().equals("")) {
            sql =
                "select k.kdKunjungan as _id, k.nama, t.tipe_pengunjung, n.nmkegiatan " +
                        "from kunjungan k, tipe t, kegiatan n " +
                        "where k.id_tipe=t.id_tipe and k.id_kegiatan=n.id_kegiatan like '%$kdKun%'"
        }else{
            sql =
                "select k.kdKunjungan as _id, k.nama, t.tipe_pengunjung, n.nmkegiatan " +
                        "from kunjungan k, tipe t, kegiatan n " +
                        "where k.id_tipe=t.id_tipe and k.id_kegiatan=n.id_kegiatan order by k.kdKunjungan asc"
        }
        val c: Cursor = db.rawQuery(sql, null)
        lsAdapter = SimpleCursorAdapter(
            thisParent, R.layout.item_data_kunjungan, c,
            arrayOf("_id", "nama", "tipe_pengunjung", "nmkegiatan"), intArrayOf(
                R.id.txKodeKunjungan,
                R.id.txNamaPengunjung,
                R.id.txTipePengunjung,
                R.id.txKegPengunjung
            ),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsKunjungan.adapter = lsAdapter
    }


    fun showDataTipe(){
        val c : Cursor = db.rawQuery("select tipe_pengunjung as _id from tipe order by tipe_pengunjung asc",null)
        adapterTipe = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        adapterTipe.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner2.adapter = adapterTipe
        v.spinner2.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrTipe.add(temp) //add the item
            c.moveToNext()
        }
    }

    fun showDataKeg(){
        val c : Cursor = db.rawQuery("select nmkegiatan as _id from kegiatan order by nmkegiatan asc",null)
        adapterKeg = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        adapterKeg.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinner3.adapter = adapterKeg
        v.spinner3.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrKeg.add(temp) //add the item
            c.moveToNext()
        }
    }

    fun insertDataKun(kdKun : String , namaKun: String, Tipe : String, Kegiatan : String){
        var sql = "insert into kunjungan (kdKunjungan,nama,id_tipe,id_kegiatan) values (?,?,?,?)"
        db.execSQL(sql, arrayOf(kdKun,namaKun,Tipe,Kegiatan))
        showDataKun("")
    }

    val btnInsertDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sqlTipe = "select id_tipe from tipe where tipe_pengunjung = '$namaTipe'"
        var sqlKeg = "select id_kegiatan from kegiatan where nmkegiatan = '$namaKeg'"
        val c1 : Cursor = db.rawQuery(sqlTipe,null)
        val c2 : Cursor = db.rawQuery(sqlKeg,null)
        if(c1.count>0 && c2.count>0){
            c1.moveToFirst()
            c2.moveToFirst()
            insertDataKun(
                v.edKodeKunjungan.text.toString(),
                v.edNamaKunjungan.text.toString(),
                c1.getString(c1.getColumnIndex("id_tipe")),
                c2.getString(c2.getColumnIndex("id_kegiatan"))
            )
            v.edKodeKunjungan.setText("")
            v.edNamaKunjungan.setText("")
            v.spinner2.setSelection(0)
            v.spinner3.setSelection(0)
        }
    }

    fun updateDataKun(kdKun : String , namaKun: String, Tipe : Int, Kegiatan : Int){
        var cv : ContentValues = ContentValues()
        cv.put("kdKunjungan",kdKun)
        cv.put("nama",namaKun)
        cv.put("id_tipe",Tipe)
        cv.put("id_kegiatan",Kegiatan)
        db.update("kunjungan",cv,"kdKunjungan = $kdKun",null)
        showDataKun("")
    }

    val btnUpdateDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sqlTipe = "select id_tipe from tipe where tipe_pengunjung = '$namaTipe'"
        var sqlKeg = "select id_kegiatan from kegiatan where nmkegiatan = '$namaKeg'"
        val c1 : Cursor = db.rawQuery(sqlTipe,null)
        val c2 : Cursor = db.rawQuery(sqlKeg,null)
        if(c1.count>0 && c2.count>0){
            c1.moveToFirst()
            c2.moveToFirst()
            updateDataKun(
                v.edKodeKunjungan.text.toString(),
                v.edNamaKunjungan.text.toString(),
                c1.getInt(c1.getColumnIndex("id_tipe")),
                c2.getInt(c2.getColumnIndex("id_kegiatan"))
            )
            v.edKodeKunjungan.setText("")
            v.edNamaKunjungan.setText("")
            v.spinner2.setSelection(0)
            v.spinner3.setSelection(0)
        }
    }

    fun deleteDataKun(kdKun: String){
        db.delete("kunjungan","kdKunjungan = $kdKun",null)
        showDataKun("")
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataKun(v.edKodeKunjungan.text.toString())
        v.edKodeKunjungan.setText("")
        v.edNamaKunjungan.setText("")
        v.spinner2.setSelection(0)
        v.spinner3.setSelection(0)
    }

}
