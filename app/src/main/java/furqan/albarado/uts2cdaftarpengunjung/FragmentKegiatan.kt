package furqan.albarado.uts2cdaftarpengunjung

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CursorAdapter
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frag_data_kegiatan.view.*

class FragmentKegiatan : Fragment(), View.OnClickListener{
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnDelete ->{
                builder.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
            R.id.btnUpdate ->{
                builder.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                builder.show()
            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var v :View
    lateinit var builder : AlertDialog.Builder
    var idKegiatan : String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        db = thisParent.getDBObject()

        v = inflater.inflate(R.layout.frag_data_kegiatan,container,false)
        v.btnUpdate.setOnClickListener(this)
        v.btnInsert.setOnClickListener(this)
        v.btnDelete.setOnClickListener(this)
        builder = AlertDialog.Builder(thisParent)
        v.lsKegiatan.setOnItemClickListener(itemClick)

        return v
    }

    fun showDataKegiatan(){
        val cursor : Cursor = db.query("kegiatan", arrayOf("nmkegiatan", "id_kegiatan as _id"),
            null, null, null,null,"nmkegiatan asc")
        adapter = SimpleCursorAdapter(thisParent,R.layout.item_data_kegiatan,cursor,
            arrayOf("_id","nmkegiatan"), intArrayOf(R.id.txldKegiatan, R.id.txKegiatan),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lsKegiatan.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        showDataKegiatan()
    }

    val itemClick = AdapterView.OnItemClickListener{ parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        idKegiatan = c.getString(c.getColumnIndex("_id"))
        v.edKegiatan.setText(c.getString(c.getColumnIndex("nmkegiatan")))
    }

    fun insertDataKegiatan(namaKegiatan : String){
        var cv : ContentValues = ContentValues()
        cv.put("nmkegiatan",namaKegiatan)
        db.insert("kegiatan", null,cv)
        showDataKegiatan()
    }

    fun updateDataKegiatan(namaKegiatan: String, idKegiatan: String){
        var cv : ContentValues = ContentValues()
        cv.put("nmkegiatan", namaKegiatan)
        db.update("kegiatan",cv,"id_kegiatan = $idKegiatan",null)
        showDataKegiatan()
    }

    fun deleteDataKegiatan(idKegiatan : String){
        db.delete("kegiatan","id_kegiatan = $idKegiatan", null)
        showDataKegiatan()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataKegiatan(v.edKegiatan.text.toString())
        v.edKegiatan.setText("")
    }
    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        updateDataKegiatan(v.edKegiatan.text.toString(), idKegiatan)
        v.edKegiatan.setText("")
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataKegiatan(idKegiatan)
        v.edKegiatan.setText("")
    }
}