package furqan.albarado.uts2cdaftarpengunjung

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpen(context: Context): SQLiteOpenHelper(context,DB_Name,null, DB_Ver) {
    companion object{
        val DB_Name = "pengunjung"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tTipe = "create table tipe(id_tipe integer primary key autoincrement, tipe_pengunjung text not null)"
        val tKegiatan = "create table kegiatan(id_kegiatan integer primary key autoincrement, nmkegiatan text not null)"
        val tKunjungan = "create table kunjungan(kdKunjungan text primary key ,nama text not null, id_tipe integer not null, id_kegiatan integer not null)"
        db?.execSQL(tTipe)
        db?.execSQL(tKegiatan)
        db?.execSQL(tKunjungan)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }
}